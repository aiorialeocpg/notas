const express = require('express');
const router = express.Router();
const User = require('../models/User');
const passport = require('passport');

router.get('/users/signin', (req,res) => {
    res.render('users/signin');
});
router.post('/users/signin', passport.authenticate('local',{
    successRedirect: '/notes',
    failureRedirect: '/users/signin',
    failureFlash: true
}));

router.get('/users/signup', (req,res) => {
    res.render('users/signup');
});
router.post('/users/signup', async (req,res) => {
    const {name,email,password,confirm_password} = req.body;
    const errors = [];
    console.log(req.body);
    if(name.length<=0){
        errors.push({text:"Ingresa el nombre"});
    }else
    if(email.length<=0){
        errors.push({text:"Ingresa el email"});
    }else
    if(password.length<=0){
        errors.push({text:"Ingresa el password"});
    }else
    if(confirm_password.length<=0){
        errors.push({text:"Ingresa la confirmacion del password"});
    }else
    if(password!=confirm_password){
        errors.push({text:"El password no coincide"});
    }else
    if(password.length < 4){
        errors.push({text:"La contraseña debe ser al menos de 4 caracteres"});
    }
    if(errors.length > 0){
        res.render('users/signup', {errors,name,email, password,confirm_password});
    }else{
        //res.send('OK');
       const emailUser =  await User.findOne({email: email});
       if (emailUser){
            req.flash('error','El email ya esta en uso!');
            res.redirect('/users/signup');
       }else{
            const newUser = new User({name,email,password});
            newUser.password =  await newUser.encryptPassword(password);
            await newUser.save();
            req.flash('success_msg','Registro existoso');
            res.redirect('/users/signin');
            //console.log(newUser.password);
       }
    }
});

router.get('/users/logout',(req,res) => {
    req.logout();
    res.redirect('/');
});

module.exports = router;